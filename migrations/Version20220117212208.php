<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220117212208 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE annonces (id INT AUTO_INCREMENT NOT NULL, category VARCHAR(50) DEFAULT NULL, owner_id_id INT DEFAULT NULL, title VARCHAR(50) NOT NULL, description LONGTEXT DEFAULT NULL, country VARCHAR(50) DEFAULT NULL, zip VARCHAR(50) DEFAULT NULL, city VARCHAR(50) DEFAULT NULL, is_valid TINYINT(1) DEFAULT \'0\' NOT NULL, is_confirm TINYINT(1) DEFAULT \'0\' NOT NULL, is_phone_visible TINYINT(1) DEFAULT \'0\' NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP, updated_at DATETIME on update CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP, UNIQUE INDEX UNIQ_CB988C6F2B36786B (title), INDEX IDX_CB988C6F64C19C1 (category), INDEX IDX_CB988C6F8FDDAB70 (owner_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categories (id VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(80) DEFAULT NULL, is_enabled TINYINT(1) DEFAULT \'0\' NOT NULL, is_moderator TINYINT(1) DEFAULT \'0\' NOT NULL, is_admin TINYINT(1) DEFAULT \'0\' NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP, updated_at DATETIME on update CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE annonces ADD CONSTRAINT FK_CB988C6F64C19C1 FOREIGN KEY (category) REFERENCES categories (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE annonces ADD CONSTRAINT FK_CB988C6F8FDDAB70 FOREIGN KEY (owner_id_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE annonces DROP FOREIGN KEY FK_CB988C6F64C19C1');
        $this->addSql('ALTER TABLE annonces DROP FOREIGN KEY FK_CB988C6F8FDDAB70');
        $this->addSql('DROP TABLE annonces');
        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE user');
    }
}
