<?php

namespace App\Controller;

use App\Entity\Annonces;
use App\Entity\Categories;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class HomeController extends AbstractController
{
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/", name="home")
     */
    public function index(EntityManagerInterface $manager): Response
    {

        $repo = $this->getDoctrine()->getRepository(Annonces::class);

        var_dump($this->getDoctrine()->getRepository(Categories::class)->findOneBy(['id' => 'Divers']));

        // var_dump($repo->findAll());
        // foreach ($repo->findAll() as $key => $value) {
        //     echo "<pre>";
        //     var_dump($value);
        //     echo "</pre>";
        // }

        // $categories = new Categories();
        // $categories->setId('bbbbb');

        $user = new User();

        $plainPassword = 'ryanpass';

        // $encoded = $encoder->encodePassword($user, $plainPassword);

        // $user->setCreatedAt()
        //     ->setUsername('padcmoi')
        //     ->setPassword($this->passwordEncoder->encodePassword($user, 'the_new_password'))
        //     ->setEmail('padcmoi@naskot.fr');
        // $manager->persist($user);
        // $manager->flush();

        $annonces = new Annonces();
        $annonces
            ->setCreatedAt()
            ->setCategory($this->getDoctrine()->getRepository(Categories::class)->findOneBy(['id' => 'Divers']))
            ->setOwnerId($this->getDoctrine()->getRepository(User::class)->findOneBy(['username' => 'padcmoi']))
            ->setTitle('Titre n°' . microtime(true) * 10000)
            ->setDescription('description ...')
            ->setIsValid(false);

        // $manager->persist($categories);
        $manager->persist($annonces);
        $manager->flush();

        $this->addFlash(
            'notice',
            'Your changes were saved!'
        );

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'ads' => $repo->findAll(),
        ]);
    }
}