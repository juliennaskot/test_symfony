<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function index(Request $request, EntityManagerInterface $manager): Response
    {
        $form = $request->request;

        if ($this->isCsrfTokenValid('authenticate', $form->get('_csrf_token'))) {
            echo $form->get('_username');
            echo $form->get('_password');
        }

        return $this->render('login/index.html.twig', []);
    }
}